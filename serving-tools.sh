#!/bin/bash
set -e
CONFIG_ENV="config.env"
source ${CONFIG_ENV}


### Variables ###
#actions
BUILD=0
CLEAN=0

### Actions ###
function build_serving {
    echo "Pull serving image"
    docker pull tensorflow/serving:2.8.2

    echo "Make Subdirs:"
    echo "Serve: ${SERVE_MODEL_DIR} and Tmp: ${TMP_MODEL_DIR}"
    mkdir -p ${SERVE_MODEL_DIR}
    mkdir -p ${TMP_MODEL_DIR}

    echo "Download TF MODEL to: ${TMP_MODEL_GZ_PATH_NAME}"
    wget -O ${TMP_MODEL_GZ_PATH_NAME} ${SERVE_MODEL_URL}

    echo "Unpack TF MODEL to: ${SERVE_MODEL_DIR}"
    tar -xzvf ${TMP_MODEL_GZ_PATH_NAME} -C ${SERVE_MODEL_DIR}
}

function rm_serving {
    echo "RM Serving data"
    rm ${TMP_MODEL_GZ_PATH_NAME}
    rm -rf ${SERVE_MODEL_DIR}
}


### Read command line arguments ###
while [ "$1" != "" ]; do
    case $1 in
        -b | --build )   BUILD=1;;
        -cl | --clean )  CLEAN=1;;
    esac
    shift
done
# check if --build and --clean flags were not provided
if [ $BUILD -eq 0 ] && [ $CLEAN -eq 0 ]; then
    echo "--build and --clean flags are required"
fi

if [ $CLEAN -eq 1 ]; then
    echo "--clean flag detected"
    rm_serving
fi

if [ $BUILD -eq 1 ]; then
    echo "--build flag detected"
    build_serving
fi
