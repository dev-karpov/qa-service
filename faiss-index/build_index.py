"""
При помощи данного файла можно построить faiss индекс
./python build_index.py -d <директория где лежат use фалйы> -v <версия dgs>

Индекс строится с Product quantization (PQ) - что бы уменьшить объем
Объем уменьшился с ~170mb при flat индекксе до ~7mb
"""

import json
import pickle
from pathlib import Path

import faiss
import numpy as np

import argparse


def build_index(embs):
    dim = embs.shape[1]
    n_centroids = np.sqrt(embs.shape[0])
    n_centroids = int(n_centroids)

    emb_len = len(embs)
    n_train_size = int(emb_len * 0.2)

    embs_ix_tr = np.random.choice(emb_len, size=n_train_size)
    embs_tr = embs[embs_ix_tr]
    print(embs_tr.shape)

    index = faiss.index_factory(dim, f"IVF{n_centroids},PQ64", faiss.METRIC_L2)
    index.train(embs_tr)
    index.add(embs)
    return index


def search_answer(question_vector, index):
    index.nprobe = 32
    topn=5
    dists, ixs = index.search(question_vector, topn) 
    return ixs


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--dir", help="the path to the directory")
    parser.add_argument("-v", "--version", help="dgs version number")
    args = parser.parse_args()

    if args.version:
        print("version:", args.version)
    if args.dir:
        print("path:", args.dir)

    clusters_use_dg_fname = Path(f'clusters_use_dg{args.version}.json')
    clusters_use_dg_path = Path(args.dir)/Path(clusters_use_dg_fname)

    use_embeddings_dg_fname = Path(f'use_embeddings_dg{args.version}.pkl')
    use_embeddings_dg_path = Path(args.dir)/Path(use_embeddings_dg_fname)

    print(clusters_use_dg_path)
    print(use_embeddings_dg_path)

    cluster2answer = None
    with open(clusters_use_dg_path) as fd:
        cluster2answer = json.load(fd)

    use_embs = None
    with open(use_embeddings_dg_path, 'rb') as fd:
        use_embs = pickle.load(fd)

    for cluster_id in range(4):
        cluster_id = str(cluster_id)
        cluster_answers = cluster2answer[cluster_id]
        cluster_embs = [use_embs[answer].squeeze() for answer in cluster_answers]
        cluster_embs = np.array(cluster_embs)

        index =  build_index(cluster_embs)
        write_ix_fname = Path(f"ix_pq_clstr{cluster_id}_dgs{args.version}.index")
        write_ix_path = Path(args.dir)/write_ix_fname
        faiss.write_index(index, str(write_ix_path))
        print(write_ix_path, cluster_id)
