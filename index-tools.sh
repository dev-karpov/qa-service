#!/bin/bash
set -e
CONFIG_ENV="config.env"
source ${CONFIG_ENV}

### Variables ###
#actions
SOURCE_DIR="."

### Parse the arguments using the getopts command
# Check if the number of arguments is correct
if [ $# -ne 4 ]; then
    echo "Usage: $0 -v version -s source"
    exit 1
fi

while getopts ":v:s:" opt; do
    case $opt in
        v)
            DGS_VER=$OPTARG
            ;;
        s)
            SOURCE_DIR=$OPTARG
            ;;
        \?)
            echo "Invalid option: -$OPTARG"
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument."
            exit 1
            ;;
    esac
done

CLUST_CENTER_FILE=${SOURCE_DIR}/"clusters_centers_use_dg${DGS_VER}.pkl"
CLUSTERS_USE_DG_FILE=${SOURCE_DIR}/"clusters_use_dg${DGS_VER}.json"
INDEXE_FILES=()
for i in $(seq 0 3)
do
    INDEXE_FILES+=(${SOURCE_DIR}/"ix_pq_clstr${i}_dgs${DGS_VER}.index")
done


echo "Path: ${SOURCE_DIR}"
echo "-> ${CLUST_CENTER_FILE}"
echo "-> ${CLUSTERS_USE_DG_FILE}"
for i in $(seq 0 3)
do
    echo "-> ${INDEXE_FILES[i]}"
done


files=(${CLUST_CENTER_FILE} ${CLUSTERS_USE_DG_FILE})
files+=(${INDEXE_FILES[@]})
for file in ${files[@]}; do
    if [ ! -f ${file} ]; then
        echo "File ${file} does not exist."
        exit 1
    else
        echo "File ${file} exists."
    fi
done

### Action ###
# делаем временную копию, из этой папки будет расслыка 
mkdir -p ${UPLOAD_TMP_DIR}
cp ${files[@]} ${UPLOAD_TMP_DIR}

# Iterate through the list of hosts
for host in ${ALL_HOSTS[@]}; do
    echo
    echo "Connect to the ${host} and create the directory ${INDEX_DIR}"
    ssh root@${host} "mkdir -p ${INDEX_DIR}"
    echo "RSync: ${UPLOAD_TMP_DIR} with ${INDEX_DIR}"
    rsync -raqzP ${UPLOAD_TMP_DIR}/* root@${host}:${INDEX_DIR}
    if [ $? -ne 0 ]; then
        # Print an error message if the rsync command failed
        echo "ERROR: rsync failed."
        exit 1
    fi
done

# удалить файлы, что бы не занимать место на диске
rm -rf ${UPLOAD_TMP_DIR}
echo
echo "DGS copy SUCCESS"
