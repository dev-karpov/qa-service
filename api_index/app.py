import os
import random
from typing import List

from pathlib import Path

import json
import faiss
import numpy as np

import uvicorn
from fastapi import FastAPI
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import List

import requests as rq

DGS_VER = os.getenv('DGS_VER') or "1"
CLUSTER_ID = os.getenv('CLUSTER_ID') or "1"
DGS_VER = str(DGS_VER)
CLUSTER_ID = str(CLUSTER_ID)

# PATHs
srv_data_dir = Path('/srv/dgs-serving')
srv_centers_path = srv_data_dir / Path(f'clusters_use_dg{DGS_VER}.json')

index_fname = Path(f"ix_pq_clstr{CLUSTER_ID}_dgs{DGS_VER}.index")
index_path = Path(srv_data_dir)/index_fname

emb2answer_path = Path(srv_data_dir)/Path(f'clusters_use_dg{DGS_VER}.json')

# INIT Data
index = faiss.read_index(str(index_path))

answers_data = None
with open(srv_centers_path, 'rb') as fd:
    answers_data = json.load(fd)

cluster2answer = None
with open(emb2answer_path) as fd:
    cluster2answer = json.load(fd)


# Index Search
def search_answer(question_vector, index):
    index.nprobe = 32
    topn=5
    dists, ixs = index.search(question_vector, topn)
    return ixs


# DECLARE API
app = FastAPI()

class QuestionEmbedding(BaseModel):
    dgs: int = Field(
        default=None, 
        description="An integer value representing the DGS parameter",
        example=1
    )
    embedding: List[float] = Field(
        default=None,
        description="List of float values representing the query embedding",
        example=[1,2,3,4,5]
    )


@app.get('/')
@app.get('/healthcheck')
async def healthcheck():
    """
    Check Index
    """
    status = {
        'status': 'ok',
        'message':'Index Works!',
        'cluster_id': CLUSTER_ID,
        'dgs': DGS_VER
    }
    return status


@app.post(f'/v{DGS_VER}/answers')
async def best_answers(question_embedding: QuestionEmbedding):
    """
    Return 5 best answers
    """
    k_best = 5
    answers = []
    client_dgs_ver = str(question_embedding.dgs)
    qry_embedding = question_embedding.embedding
    req_resp = {
        'status':'ok',
        'dgs': DGS_VER
    }

    # Check if a new version of the DGS is available.
    if client_dgs_ver != DGS_VER:
        return JSONResponse({
                "error": "A new version of the DGS is available. Please upgrade"
            }, 
            status_code=426
        )
    
    emb_length = len(qry_embedding)
    if len(qry_embedding) != 512:
        return JSONResponse({
            "error": f"Emb has an incorrect {emb_length} length. Please provide 512 length and try again."
            }, 
            status_code=400
        )

    qry_embedding = np.array(qry_embedding).astype(np.float32).reshape(1,-1)
    ixs = search_answer(qry_embedding, index)
    cluster_answers = cluster2answer[CLUSTER_ID]
    answers = np.array(cluster_answers)[ixs]
    answers = answers.squeeze().tolist()
    req_resp['answers'] = answers

    return req_resp


if __name__ == "__main__":
    host='0.0.0.0'
    port=5000
    uvicorn.run(app, host=host, port=port)
