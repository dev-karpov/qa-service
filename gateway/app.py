import os
from pathlib import Path

import json
import pickle
import numpy as np

import uvicorn
from fastapi import FastAPI
from fastapi.responses import JSONResponse
import requests as rq

DGS_VER = os.getenv('DGS_VER')
DGS_VER = str(DGS_VER)

host = 'host.docker.internal'
port_rest = 8501
model = 'use-light'

index_host='host.docker.internal'
index_host='65.108.87.37'
port_index_pattern='1993{cluster_id}'


srv_data_dir = Path('/srv/dgs-serving')
srv_centers_path = srv_data_dir / Path(f'clusters_centers_use_dg{DGS_VER}.pkl')

clust_centers = None
with open(srv_centers_path, 'rb') as fd:
    clust_centers = pickle.load(fd)

centers_mtx = np.array([
    center_vec for center_vec in clust_centers.values()
])

app = FastAPI()


@app.get('/')
@app.get('/healthcheck')
async def healthcheck():
    """
    healthcheck GateWay
    """
    for cluster_id in [0,1,2,3]:
        port_api = port_index_pattern.format(cluster_id=cluster_id)
        qry_index_url = f'http://{index_host}:{port_api}/healthcheck'
        resp = rq.get(qry_index_url)
        if resp.status_code != 200:
            error_code = resp.status_code
            return JSONResponse({
                    'error': f'index_api_{cluster_id} not working',
                }, 
                status_code=error_code
            )
        
        data = resp.json()
        index_api_dgs_version = data['dgs']
        if DGS_VER != index_api_dgs_version:
            return JSONResponse({
                    'error': f'index_api_{cluster_id} mismatch dgs version',
                }, 
                status_code=500
            )
        index_api_cluster_id = data['cluster_id']
        cluster_id = str(cluster_id)
        index_api_cluster_id = str(index_api_cluster_id)
        if cluster_id != index_api_cluster_id:
            return JSONResponse({
                    'error': f'index_api_{cluster_id} mismatch cluster_id',
                }, 
                status_code=500
            )

    qry_emb_serving_url = f'http://{host}:{port_rest}/v1/models/{model}/metadata'
    resp = rq.get(qry_emb_serving_url)
    if resp.status_code != 200:
        error_code = resp.status_code
        return JSONResponse({
                'error': 'tf serving_api not working',
            }, 
            status_code=error_code
        )  

    status = {
        'status': 'ok',
        'message':'GateWay Works!',
        'dgs': DGS_VER
    }
    return status


@app.get(f"/v{DGS_VER}/answer")
async def answer_question(question: str):
    req_resp = {
        'status':'ok'
    }

    if not isinstance(question, str):
        req_resp['status'] = 'false'
        req_resp['msg'] = 'wrong question type'
        return req_resp

    data = {
            "instances":  [question],
    }
    qry_emb_url = f'http://{host}:{port_rest}/v1/models/{model}:predict'
    qry_resp = rq.post(
        qry_emb_url, 
        json=data
    ).json()

    question_emb = qry_resp['predictions'][0]
    question_emb = np.array(question_emb)

    # search cluster
    u_v = np.sum(question_emb*centers_mtx, axis=1)
    abs_u = np.sqrt(np.sum(question_emb * question_emb))
    abs_v = np.sqrt(np.sum(centers_mtx * centers_mtx, axis=1))
    cos_sims = u_v / (abs_u * abs_v)
    question_cluster = np.argmin(cos_sims)
    question_cluster = str(question_cluster)
    

    port=port_index_pattern.format(cluster_id=question_cluster)
    qry_ans_url = f'http://{index_host}:{port}/v{DGS_VER}/answers'
    data =  {
        'dgs': DGS_VER,
        'embedding': list(question_emb)
    }
    index_resp = rq.post(
        qry_ans_url, 
        json=data
    )
    index_resp_json = index_resp.json()

    if index_resp.status_code != 200:
        error_code = index_resp.status_code
        return JSONResponse(
            index_resp_json, 
            status_code=error_code
        )

    answers = index_resp_json['answers']

    req_resp["answers"] = answers
    req_resp["question"] = question
    req_resp["cluster_id"] = question_cluster
    # req_resp["predictions shape"] = str(question_emb.shape)
    req_resp["dgs_version"] = DGS_VER

    return req_resp


if __name__ == "__main__":
    host='0.0.0.0'
    port=5000
    uvicorn.run(app, host=host, port=port)
