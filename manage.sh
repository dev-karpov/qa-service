#!/bin/bash
set -e
CONFIG_ENV="config.env"
source ${CONFIG_ENV}

### Variables ###
#actions
BUILD=0
RUN=0
UPDATE=0
STOP=0

### Functions ###
# собирает образ для gateway и отправляет в registry
build_gateway() {
    docker build -t gateway ./gateway
    docker tag gateway ${DOCKER_REGISTRY}/gateway:latest
    docker push ${DOCKER_REGISTRY}/gateway:latest
}

# собирает образ для api_index и отправляет в registry
build_api_index() {
    docker build -t api_index ./api_index
    docker tag api_index ${DOCKER_REGISTRY}/api_index:latest
    docker push ${DOCKER_REGISTRY}/api_index:latest
}

# обновляет индекс и сервисы согласно переданной версии
update_services() {
    bash ./index-tools.sh -s ${SOURCE_DIR} -v ${VERSION}
    if [ $? -ne 0 ]; then
        # Print an error message if the rsync command failed
        echo "Error: index update failed."
        exit 1
    fi

    for service in ${SERVICES[@]}; do
        echo
        docker service update \
                    --update-delay 2s \
                    --update-failure-action=rollback \
                    --update-parallelism 2 \
                    --env-add DGS_VER=${VERSION} \
                    ${service}
    done
}

# проверит версии data generation сервисов после обновления
# если всё ок - вернет 1
# если есть проблемы - вернет 0
check_update() {
    update_success=1
    bash ./check_api_dgs.sh -v ${VERSION}
    
    if [ $? -ne 0 ]; then
        echo "ERR: DGS doesnt Match"
        update_success=0
    fi
    return ${update_success}
}

loadenvs() {
    set -a && . $CONFIG_ENV && set +a
}

### Read command line arguments ###
while [ "$1" != "" ]; do
    case $1 in
        -r | --run )   RUN=1;;
        -s | --stop )  STOP=1;;
        -b | --build )  BUILD=1;;
        -u | --update )  UPDATE=1;;
        -v | --version ) shift 
            VERSION=$1;;
    esac
    shift
done
# check if --build and --clean flags were not provided
if [ $RUN -eq 0 ] && [ $STOP -eq 0 ] && [ $BUILD -eq 0 ] && [ $VERSION -eq 0 ]; then
    echo "--run, --stop, --build or --update flags are required"
    exit 1
fi

if [ $STOP -eq 1 ]; then
    echo "--stop flag detected"
    docker stack rm qa    
fi

if [ $BUILD -eq 1 ]; then
    echo "--build flag detected"
    echo "Build Gateway"
    build_gateway
    echo "Build Api Index"
    build_api_index
fi

if [ $RUN -eq 1 ]; then
    echo "--run flag detected"
    ## loadenvs нужен, что б закинуть DGS_VER и переменные в контейнеры
    loadenvs && docker stack deploy --compose-file docker-compose.yml qa
    # docker stack deploy --compose-file docker-compose.yml qa
    # docker stack deploy -c <(docker compose --env-file config.env -f docker-compose.yml config) qa
fi


if [ $UPDATE -eq 1 ]; then
    echo "--update flag detected with v${VERSION}"
    update_services
    check_update
    update_success=$?
    echo $update_success
    if [ $update_success -eq 0 ]; then
        echo "Update Failed"
        exit 1
    else
        echo "Update Success"
    fi
fi
