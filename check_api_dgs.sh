#!/bin/bash
set -e

### VARIABLES ###
HOST="65.108.87.37"
API_PORTS="19911 19930 19931 19932 19933"
SERVICES=("qa_gateway" "qa_api_index_0" "qa_api_index_1" "qa_api_index_2" "qa_api_index_3")

update_success=1
version="1"
with_opts=0

### Parse the arguments using the getopts command
while getopts ":v:" opt; do
    case $opt in
        v)
            version=$OPTARG
            with_opts=1
            ;;
        \?)
            echo "Invalid option: -$OPTARG"
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument."
            exit 1
            ;;
    esac
done

### Services Health Check
index=0
for port in ${API_PORTS}; do
    service=${SERVICES[index]}
    service_endpoint="http://${HOST}:${port}"
    error=false
    json_data=$(curl -s ${service_endpoint})|| error=true
    fact_dgs_ver="err"
    if ! $error; then
        fact_dgs_ver=$(echo ${json_data} | jq -r '.dgs')
    else
        fact_dgs_ver=-1
    fi

    output="${service_endpoint}| ${service} | dgs version:${fact_dgs_ver}"
    if [ "${version}" = "${fact_dgs_ver}" ]; then
        output="$output \e[32m OK \e[0m"
    elif [ $with_opts -eq 1 ]; then
        output="$output \e[31m FAIL \e[0m"
        update_success=0
    fi
    echo -e $output
    index=$((index+1))
done

echo
if [ $update_success -eq 0 ]; then
    echo "FAILED"
    exit 1
else
    echo "SUCCESS"
    exit 0
fi