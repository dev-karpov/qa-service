### Register gitlab ci\cd
```sh
docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
```

### Add token from:
https://gitlab.com/dev-karpov/qa-service/-/settings/ci_cd
1. host: https://gitlab.com/
2. description, tags - можно оставить пустым
3. mode: docker

#### Добавить Runner
в файл
```sh
/srv/gitlab-runner/config/config.toml
```
поменять privileged и volumes согласно файлу ниже
```ini
[[runners]]
  name = "704ab6f2fc4f"
  url = "https://gitlab.com/"
  id = 19527136
  token = "-----"
  token_obtained_at = 2022-12-02T14:00:23Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "python:3.9"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock","/cache"]
    shm_size = 0
```

Отключить Shared Runners на Gitlab:
```url
https://gitlab.com/dev-karpov/qa-service/-/settings/ci_cd
```

## Сервинг
запустить init.sh
```sh
./serving-tools.sh --clean --build
```
params
```
--clean: delete old files
--build: pull docker, download and save embs 
```

### Установить
```sh
apt install jq -y
```




# Инструкции \ Черновик
Получить докер образ для сервинга
```sh
docker pull tensorflow/serving:2.8.2
```

Скачать эмбединг
```sh
wget -O model.tar.gz "https://tfhub.dev/google/universal-sentence-encoder-multilingual-large/3?tf-hub-format=compressed" 
```

Подготовить папки и распаковать 
```sh
export MODELS_DIR=${PWD}/models/use-light/1
mkdir -p ${MODELS_DIR}
tar -xzvf model.tar.gz -C ${MODELS_DIR}
```

Запустить контейнер
```sh
docker run -it --rm -p 8501:8501 \
             -v "${PWD}/models:/models" \
             -e MODEL_NAME="use-light" \
             tensorflow/serving:2.8.2
```

Протестировать
```sh
curl -d '{"instances": ["How you doing?"]}' -X POST http://65.108.87.37:8501/v1/models/use-light:predict
```


### Черновик

```sh
docker service update --env-add DGS_VER=2 qa_gateway

docker service update --force --update-parallelism 1 --update-delay 30s qa_gateway
```